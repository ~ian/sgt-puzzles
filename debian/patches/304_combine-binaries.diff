Author: Ben Hutchings <ben@decadent.org.uk>
Description: Combine puzzles into a single executable

Link all the puzzles into a single executable and use argv[0]
to decide which to run.  This saves a large amount of disk
space due to the large amount of code that is otherwise
statically linked into multiple executables.

--- a/gtk.c
+++ b/gtk.c
@@ -2,6 +2,8 @@
  * gtk.c: GTK front end for my puzzle collection.
  */
 
+#define _GNU_SOURCE
+
 #include <stdio.h>
 #include <assert.h>
 #include <stdlib.h>
@@ -46,6 +48,14 @@
 
 /* #undef USE_CAIRO */
 /* #define NO_THICK_LINE */
+
+#ifdef COMBINED
+static const gameindex *thegameindex;
+#define thegame (*thegameindex->game)
+#define xpm_icons (thegameindex->xpm_icons)
+#define n_xpm_icons (*thegameindex->n_xpm_icons)
+#endif
+
 #ifdef DEBUGGING
 static FILE *debug_fp = NULL;
 
@@ -2190,8 +2200,6 @@ static frontend *new_window(char *arg, i
     GList *iconlist;
     int x, y, n;
     char errbuf[1024];
-    extern char *const *const xpm_icons[];
-    extern const int n_xpm_icons;
 
     fe = snew(frontend);
 
@@ -2585,7 +2593,8 @@ char *fgetline(FILE *fp)
 
 int main(int argc, char **argv)
 {
-    char *pname = argv[0];
+    char *pname;
+    int i;
     char *error;
     int ngenerate = 0, print = FALSE, px = 1, py = 1;
     int time_generation = FALSE, test_solve = FALSE, list_presets = FALSE;
@@ -2601,6 +2610,28 @@ int main(int argc, char **argv)
     char **av = argv;
     char errbuf[500];
 
+#ifdef COMBINED
+    pname = strrchr(argv[0], '/');
+    if (pname)
+	pname++;
+    else
+	pname = argv[0];
+    if (!strncmp(pname, "sgt-", 4))
+	pname += 4;
+    for (i = 0; i < gamecount; i++) {
+	size_t len = strlen(gamelist[i].name);
+	if (!strncmp(pname, gamelist[i].name, len) &&
+	    (pname[len] == 0 || !strcmp(pname + len, "game"))) {
+	    thegameindex = &gamelist[i];
+	    break;
+	}
+    }
+    if (!thegameindex) {
+	fprintf(stderr, "puzzles: unknown puzzle '%s'\n", pname);
+	exit(2);
+    }
+#endif
+
     /*
      * Command line parsing in this function is rather fiddly,
      * because GTK wants to have a go at argc/argv _first_ - and
--- a/Recipe
+++ b/Recipe
@@ -23,7 +23,7 @@ WINDOWS_COMMON = printing
          + user32.lib gdi32.lib comctl32.lib comdlg32.lib winspool.lib
 WINDOWS  = windows WINDOWS_COMMON
 COMMON   = midend drawing misc malloc random version
-GTK      = gtk printing ps
+GTK      = gtk[COMBINED] printing ps
 # Objects needed for auxiliary command-line programs.
 STANDALONE = nullfe random misc malloc
 
@@ -39,6 +39,7 @@ ALL      = list
  * it directly, or the changes will be lost next time mkfiles.pl runs.
  * Instead, edit Recipe and/or its *.R subfiles.
  */
+#define COMBINED
 #include "puzzles.h"
 #define GAMELIST(A) \
 !end
@@ -49,10 +50,13 @@ ALL      = list
 # Then we finish up list.c as follows:
 !begin >list.c
 
-#define DECL(x) extern const game x;
-#define REF(x) &x,
+#define DECL(x)						\
+extern const game x;					\
+extern const char *const *const x##_xpm_icons[];	\
+extern const int x##_n_xpm_icons;
+#define REF(x) { #x, &x, x##_xpm_icons, &x##_n_xpm_icons },
 GAMELIST(DECL)
-const game *gamelist[] = { GAMELIST(REF) };
+const gameindex gamelist[] = { GAMELIST(REF) };
 const int gamecount = lenof(gamelist);
 !end
 
@@ -92,6 +96,12 @@ Puzzles.dmg: Puzzles
 	rm -f raw.dmg devicename
 !end
 
+# Gtk unified application containing all the puzzles.
+puzzles  : [X] GTK COMMON ALL ALL_ICONS
+!begin gtk
+%-icon.o : override CFLAGS += -Dxpm_icons=$(@:%-icon.o=%)_xpm_icons -Dn_xpm_icons=$(@:%-icon.o=%)_n_xpm_icons
+!end
+
 # Version management.
 !begin vc
 version.obj: *.c *.h
@@ -177,8 +187,11 @@ version2.def: FORCE
 # make install for Unix.
 !begin gtk
 install:
+	mkdir -p $(DESTDIR)$(libdir)/sgt-puzzles
+	$(INSTALL_PROGRAM) -m 755 puzzles \
+		$(DESTDIR)$(libdir)/sgt-puzzles/puzzles
 	for i in $(GAMES); do \
-		$(INSTALL_PROGRAM) -m 755 $(BINPREFIX)$$i $(DESTDIR)$(gamesdir)/$(BINPREFIX)$$i \
+		ln -s $(libdir)/sgt-puzzles/puzzles $(DESTDIR)$(gamesdir)/$(BINPREFIX)$$i \
 		|| exit 1; \
 	done
 !end
--- a/mkfiles.pl
+++ b/mkfiles.pl
@@ -1126,6 +1126,7 @@ if (defined $makefiles{'gtk'}) {
     "prefix=/usr\n",
     "exec_prefix=\$(prefix)\n",
     "bindir=\$(exec_prefix)/bin\n",
+    "libdir=\$(exec_prefix)/lib\n",
     "gamesdir=\$(exec_prefix)/games\n",
     "sharedir=\$(prefix)/share\n",
     "mandir=\$(sharedir)/man\n",
--- a/puzzles.h
+++ b/puzzles.h
@@ -581,7 +581,13 @@ struct drawing_api {
  * there's a list of all available puzzles in array form.
  */
 #ifdef COMBINED
-extern const game *gamelist[];
+typedef struct {
+    const char *name;
+    const game *game;
+    const char *const *const *xpm_icons;
+    const int *n_xpm_icons;
+} gameindex;
+extern const gameindex gamelist[];
 extern const int gamecount;
 #else
 extern const game thegame;
--- a/blackbox.R
+++ b/blackbox.R
@@ -5,6 +5,7 @@ blackbox : [X] GTK COMMON blackbox black
 blackbox : [G] WINDOWS COMMON blackbox blackbox.res|noicon.res
 
 ALL += blackbox[COMBINED]
+ALL_ICONS += blackbox-icon
 
 !begin gtk
 GAMES += blackbox
--- a/bridges.R
+++ b/bridges.R
@@ -7,6 +7,7 @@ bridges  : [X] GTK COMMON bridges BRIDGE
 bridges  : [G] WINDOWS COMMON bridges BRIDGES_EXTRA bridges.res|noicon.res
 
 ALL += bridges[COMBINED] BRIDGES_EXTRA
+ALL_ICONS += bridges-icon
 
 !begin gtk
 GAMES += bridges
--- a/cube.R
+++ b/cube.R
@@ -5,6 +5,7 @@ cube     : [X] GTK COMMON cube cube-icon
 cube     : [G] WINDOWS COMMON cube cube.res|noicon.res
 
 ALL += cube[COMBINED]
+ALL_ICONS += cube-icon
 
 !begin gtk
 GAMES += cube
--- a/dominosa.R
+++ b/dominosa.R
@@ -7,6 +7,7 @@ dominosa : [X] GTK COMMON dominosa DOMIN
 dominosa : [G] WINDOWS COMMON dominosa DOMINOSA_EXTRA dominosa.res|noicon.res
 
 ALL += dominosa[COMBINED] DOMINOSA_EXTRA
+ALL_ICONS += dominosa-icon
 
 !begin gtk
 GAMES += dominosa
--- a/fifteen.R
+++ b/fifteen.R
@@ -5,6 +5,7 @@ fifteen  : [X] GTK COMMON fifteen fiftee
 fifteen  : [G] WINDOWS COMMON fifteen fifteen.res|noicon.res
 
 ALL += fifteen[COMBINED]
+ALL_ICONS += fifteen-icon
 
 !begin gtk
 GAMES += fifteen
--- a/filling.R
+++ b/filling.R
@@ -10,6 +10,7 @@ filling : [X] GTK COMMON filling FILLING
 filling : [G] WINDOWS COMMON filling FILLING_EXTRA filling.res|noicon.res
 
 ALL += filling[COMBINED] FILLING_EXTRA
+ALL_ICONS += filling-icon
 
 !begin gtk
 GAMES += filling
--- a/flip.R
+++ b/flip.R
@@ -7,6 +7,7 @@ flip     : [X] GTK COMMON flip FLIP_EXTR
 flip     : [G] WINDOWS COMMON flip FLIP_EXTRA flip.res|noicon.res
 
 ALL += flip[COMBINED] FLIP_EXTRA
+ALL_ICONS += flip-icon
 
 !begin gtk
 GAMES += flip
--- a/galaxies.R
+++ b/galaxies.R
@@ -14,6 +14,7 @@ galaxiespicture : [U] galaxies[STANDALON
 galaxiespicture : [C] galaxies[STANDALONE_PICTURE_GENERATOR] GALAXIES_EXTRA STANDALONE
 
 ALL += galaxies[COMBINED] GALAXIES_EXTRA
+ALL_ICONS += galaxies-icon
 
 !begin gtk
 GAMES += galaxies
--- a/guess.R
+++ b/guess.R
@@ -5,6 +5,7 @@ guess    : [X] GTK COMMON guess guess-ic
 guess    : [G] WINDOWS COMMON guess guess.res|noicon.res
 
 ALL += guess[COMBINED]
+ALL_ICONS += guess-icon
 
 !begin gtk
 GAMES += guess
--- a/inertia.R
+++ b/inertia.R
@@ -5,6 +5,7 @@ inertia  : [X] GTK COMMON inertia inerti
 inertia  : [G] WINDOWS COMMON inertia inertia.res|noicon.res
 
 ALL += inertia[COMBINED]
+ALL_ICONS += inertia-icon
 
 !begin gtk
 GAMES += inertia
--- a/lightup.R
+++ b/lightup.R
@@ -10,6 +10,7 @@ lightupsolver : [U] lightup[STANDALONE_S
 lightupsolver : [C] lightup[STANDALONE_SOLVER] LIGHTUP_EXTRA STANDALONE
 
 ALL += lightup[COMBINED] LIGHTUP_EXTRA
+ALL_ICONS += lightup-icon
 
 !begin gtk
 GAMES += lightup
--- a/loopy.R
+++ b/loopy.R
@@ -17,6 +17,7 @@ loopysolver :   [C] loopy[STANDALONE_SOL
 
 
 ALL += loopy[COMBINED] LOOPY_EXTRA
+ALL_ICONS += loopy-icon
 
 !begin gtk
 GAMES += loopy
--- a/map.R
+++ b/map.R
@@ -10,6 +10,7 @@ mapsolver :     [U] map[STANDALONE_SOLVE
 mapsolver :     [C] map[STANDALONE_SOLVER] MAP_EXTRA STANDALONE
 
 ALL += map[COMBINED] MAP_EXTRA
+ALL_ICONS += map-icon
 
 !begin gtk
 GAMES += map
--- a/mines.R
+++ b/mines.R
@@ -10,6 +10,7 @@ mineobfusc :    [U] mines[STANDALONE_OBF
 mineobfusc :    [C] mines[STANDALONE_OBFUSCATOR] MINES_EXTRA STANDALONE
 
 ALL += mines[COMBINED] MINES_EXTRA
+ALL_ICONS += mines-icon
 
 !begin gtk
 GAMES += mines
--- a/net.R
+++ b/net.R
@@ -9,6 +9,7 @@ net      : [X] GTK COMMON net NET_EXTRA
 netgame  : [G] WINDOWS COMMON net NET_EXTRA net.res|noicon.res
 
 ALL += net[COMBINED] NET_EXTRA
+ALL_ICONS += net-icon
 
 !begin gtk
 GAMES += net
--- a/netslide.R
+++ b/netslide.R
@@ -7,6 +7,7 @@ netslide : [X] GTK COMMON netslide NETSL
 netslide : [G] WINDOWS COMMON netslide NETSLIDE_EXTRA netslide.res|noicon.res
 
 ALL += netslide[COMBINED] NETSLIDE_EXTRA
+ALL_ICONS += netslide-icon
 
 !begin gtk
 GAMES += netslide
--- a/pattern.R
+++ b/pattern.R
@@ -8,6 +8,7 @@ patternsolver : [U] pattern[STANDALONE_S
 patternsolver : [C] pattern[STANDALONE_SOLVER] STANDALONE
 
 ALL += pattern[COMBINED]
+ALL_ICONS += pattern-icon
 
 !begin gtk
 GAMES += pattern
--- a/pegs.R
+++ b/pegs.R
@@ -7,6 +7,7 @@ pegs     : [X] GTK COMMON pegs PEGS_EXTR
 pegs     : [G] WINDOWS COMMON pegs PEGS_EXTRA pegs.res|noicon.res
 
 ALL += pegs[COMBINED] PEGS_EXTRA
+ALL_ICONS += pegs-icon
 
 !begin gtk
 GAMES += pegs
--- a/rect.R
+++ b/rect.R
@@ -5,6 +5,7 @@ rect     : [X] GTK COMMON rect rect-icon
 rect     : [G] WINDOWS COMMON rect rect.res|noicon.res
 
 ALL += rect[COMBINED]
+ALL_ICONS += rect-icon
 
 !begin gtk
 GAMES += rect
--- a/samegame.R
+++ b/samegame.R
@@ -5,6 +5,7 @@ samegame : [X] GTK COMMON samegame sameg
 samegame : [G] WINDOWS COMMON samegame samegame.res|noicon.res
 
 ALL += samegame[COMBINED]
+ALL_ICONS += samegame-icon
 
 !begin gtk
 GAMES += samegame
--- a/sixteen.R
+++ b/sixteen.R
@@ -5,6 +5,7 @@ sixteen  : [X] GTK COMMON sixteen sixtee
 sixteen  : [G] WINDOWS COMMON sixteen sixteen.res|noicon.res
 
 ALL += sixteen[COMBINED]
+ALL_ICONS += sixteen-icon
 
 !begin gtk
 GAMES += sixteen
--- a/slant.R
+++ b/slant.R
@@ -10,6 +10,7 @@ slantsolver :   [U] slant[STANDALONE_SOL
 slantsolver :   [C] slant[STANDALONE_SOLVER] SLANT_EXTRA STANDALONE
 
 ALL += slant[COMBINED] SLANT_EXTRA
+ALL_ICONS += slant-icon
 
 !begin gtk
 GAMES += slant
--- a/solo.R
+++ b/solo.R
@@ -10,6 +10,7 @@ solosolver :    [U] solo[STANDALONE_SOLV
 solosolver :    [C] solo[STANDALONE_SOLVER] SOLO_EXTRA STANDALONE
 
 ALL += solo[COMBINED] SOLO_EXTRA
+ALL_ICONS += solo-icon
 
 !begin gtk
 GAMES += solo
--- a/tents.R
+++ b/tents.R
@@ -7,6 +7,7 @@ tents    : [X] GTK COMMON tents TENTS_EX
 tents    : [G] WINDOWS COMMON tents TENTS_EXTRA tents.res|noicon.res
 
 ALL += tents[COMBINED] TENTS_EXTRA
+ALL_ICONS += tents-icon
 
 tentssolver :   [U] tents[STANDALONE_SOLVER] TENTS_EXTRA STANDALONE
 tentssolver :   [C] tents[STANDALONE_SOLVER] TENTS_EXTRA STANDALONE
--- a/twiddle.R
+++ b/twiddle.R
@@ -5,6 +5,7 @@ twiddle  : [X] GTK COMMON twiddle twiddl
 twiddle  : [G] WINDOWS COMMON twiddle twiddle.res|noicon.res
 
 ALL += twiddle[COMBINED]
+ALL_ICONS += twiddle-icon
 
 !begin gtk
 GAMES += twiddle
--- a/unequal.R
+++ b/unequal.R
@@ -13,6 +13,7 @@ latincheck : [U] latin[STANDALONE_LATIN_
 latincheck : [C] latin[STANDALONE_LATIN_TEST] tree234 maxflow STANDALONE
 
 ALL += unequal[COMBINED] UNEQUAL_EXTRA
+ALL_ICONS += unequal-icon
 
 !begin gtk
 GAMES += unequal
--- a/untangle.R
+++ b/untangle.R
@@ -7,6 +7,7 @@ untangle : [X] GTK COMMON untangle UNTAN
 untangle : [G] WINDOWS COMMON untangle UNTANGLE_EXTRA untangle.res|noicon.res
 
 ALL += untangle[COMBINED] UNTANGLE_EXTRA
+ALL_ICONS += untangle-icon
 
 !begin gtk
 GAMES += untangle
--- a/keen.R
+++ b/keen.R
@@ -11,6 +11,7 @@ keensolver : [U] keen[STANDALONE_SOLVER]
 keensolver : [C] keen[STANDALONE_SOLVER] latin[STANDALONE_SOLVER] KEEN_LATIN_EXTRA STANDALONE
 
 ALL += keen[COMBINED] KEEN_EXTRA
+ALL_ICONS += keen-icon
 
 !begin gtk
 GAMES += keen
--- a/magnets.R
+++ b/magnets.R
@@ -10,6 +10,7 @@ magnetssolver :     [U] magnets[STANDALO
 magnetssolver :     [C] magnets[STANDALONE_SOLVER] MAGNETS_EXTRA STANDALONE
 
 ALL += magnets[COMBINED] MAGNETS_EXTRA
+ALL_ICONS += magnets-icon
 
 !begin gtk
 GAMES += magnets
--- a/singles.R
+++ b/singles.R
@@ -6,6 +6,7 @@ singles : [X] GTK COMMON singles SINGLES
 singles : [G] WINDOWS COMMON singles SINGLES_EXTRA singles.res|noicon.res
 
 ALL += singles[COMBINED] SINGLES_EXTRA
+ALL_ICONS += singles-icon
 
 singlessolver : [U] singles[STANDALONE_SOLVER] SINGLES_EXTRA STANDALONE
 singlessolver : [C] singles[STANDALONE_SOLVER] SINGLES_EXTRA STANDALONE
--- a/towers.R
+++ b/towers.R
@@ -11,6 +11,7 @@ towerssolver : [U] towers[STANDALONE_SOL
 towerssolver : [C] towers[STANDALONE_SOLVER] latin[STANDALONE_SOLVER] TOWERS_LATIN_EXTRA STANDALONE
 
 ALL += towers[COMBINED] TOWERS_EXTRA
+ALL_ICONS += towers-icon
 
 !begin gtk
 GAMES += towers
--- a/signpost.R
+++ b/signpost.R
@@ -9,6 +9,7 @@ signpostsolver : [U] signpost[STANDALONE
 signpostsolver : [C] signpost[STANDALONE_SOLVER] SIGNPOST_EXTRA STANDALONE
 
 ALL += signpost[COMBINED] SIGNPOST_EXTRA
+ALL_ICONS += signpost-icon
 
 !begin gtk
 GAMES += signpost
--- a/range.R
+++ b/range.R
@@ -5,6 +5,7 @@ range    : [X] GTK COMMON range range-ic
 range    : [G] WINDOWS COMMON range range.res|noicon.res
 
 ALL += range[COMBINED]
+ALL_ICONS += range-icon
 
 !begin gtk
 GAMES += range
--- a/pearl.R
+++ b/pearl.R
@@ -9,6 +9,7 @@ pearlbench     : [U] pearl[STANDALONE_SO
 pearlbench     : [C] pearl[STANDALONE_SOLVER] PEARL_EXTRA STANDALONE
 
 ALL += pearl[COMBINED] PEARL_EXTRA
+ALL_ICONS += pearl-icon
 
 !begin gtk
 GAMES += pearl
--- a/undead.R
+++ b/undead.R
@@ -4,6 +4,7 @@ undead : [X] GTK COMMON undead undead-ic
 undead : [G] WINDOWS COMMON undead undead.res|noicon.res
 
 ALL += undead[COMBINED]
+ALL_ICONS += undead-icon
 
 !begin gtk
 GAMES += undead
--- a/unruly.R
+++ b/unruly.R
@@ -7,6 +7,7 @@ unrulysolver : [U] unruly[STANDALONE_SOL
 unrulysolver : [C] unruly[STANDALONE_SOLVER] STANDALONE
 
 ALL += unruly[COMBINED]
+ALL_ICONS += unruly-icon
 
 !begin gtk
 GAMES += unruly
