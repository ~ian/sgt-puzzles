sgt-puzzles (20170923.ff218728-0+iwj2~3.gbpc58e0c) UNRELEASED; urgency=medium

  ** SNAPSHOT build @c58e0c245845ba619c52ed1f63bd6db4388d729a **

  [ Simon Tatham ]
  * Avoid macro-generating a trailing comma in an enum.

  [ Ian Jackson ]
  * draw_thick_line: Bound thickness by 1.0 below
  * tracks: Scale thickness of "no track here" crosses
  * tracks: Roughly double the thickness of the "no track" crosses
  * RFH: tracks: Grid line width problems
  * draw_thick_line: Bound thickness by 1.0 below
  * tracks: Scale thickness of "no track here" crosses
  * tracks: Roughly double the thickness of the "no track" crosses

  [ Simon Tatham ]
  * Fix a typo in devel.but.
  * tracks: thicken the lines of the grid itself.
  * Net: reference-count the barriers array.

  [ Ian Jackson ]
  * Revert "RFH: tracks: Grid line width problems"
  * tracks.c: draw_clue: Introduce bg parameter
  * tracks: Greyscale colour initialisation: line up columns
  * tracks: Make error clue background white
  * midend: Allow "new game" to be undone

  [ Simon Tatham ]
  * Net: rewrite the drawing code for better scalability.
  * Fix auto-selection of presets in GTK.
  * Fix changing puzzle size in a maximised GTK3 window.

  [ Ian Jackson ]
  * tracks.c: draw_clue: Introduce bg parameter
  * tracks: Greyscale colour initialisation: line up columns
  * tracks: Make error clue background white

  [ Simon Tatham ]
  * deserialise: use the right one of {,c}params.
  * midend_deserialise: keep deserialised data in a struct.
  * midend_deserialise: accept an extra validation function.

  [ Ian Jackson ]
  * midend: Allow "new game" to be undone

  [ Simon Tatham ]
  * Style tweaks to the newgame_undo patch.
  * Forbid undo of new-game if it would change the params.
  * Make newgame_undo_buf 'char *', not 'void *'.
  * Fix an int->pointer cast warning in windows.c.
  * New name UI_UPDATE for interpret_move's return "".
  * Use a proper union in struct config_item.
  * Return error messages as 'const char *', not 'char *'.
  * Assorted char * -> const char * API changes.
  * Make the code base clean under -Wwrite-strings.

  [ Ian Jackson ]

 -- Ian Jackson <ian@zealot.relativity.greenend.org.uk>  Tue, 03 Oct 2017 20:48:57 +0100

sgt-puzzles (20170923.ff218728-0+iwj1) unstable; urgency=medium

  * merge from upstream
  * remove -pedantic
  * revert dependency on newer halibut

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Sun, 24 Sep 2017 14:42:38 +0100

sgt-puzzles (20161228.7cae89f-1+iwj) unstable; urgency=medium

  * rebuild for jessie

 -- Ian Jackson <ijackson@chiark.greenend.org.uk>  Sun, 24 Sep 2017 14:06:19 +0100

sgt-puzzles (20161228.7cae89f-1) unstable; urgency=medium

  * New upstream version
  * debian/rules: Generate menu file automatically, fixing the omission
    of Undead and Unruly (Closes: #832797)
  * Use debhelper compatibility level 9
  * debian/control: Update Standards-Version to 3.9.8; no changes needed
  * Build with Gtk+ 3

 -- Ben Hutchings <ben@decadent.org.uk>  Tue, 17 Jan 2017 23:57:33 +0000

sgt-puzzles (20160429.b31155b-1) unstable; urgency=medium

  * New upstream version (Closes: #791982)
    - Add Flood, a flood-filling puzzle
    - Add Palisade by Jonas Kölker, an implementation of 'Five Cells'
    - Add Tracks by James Harvey, a path-finding railway track puzzle
  * debian/control: Change Vcs-Git and Vcs-Browser to HTTP-S URLs
  * Fix HTML filenames for Rectangles (Closes: #819906)
  * debian/rules: Use dpkg makefile fragments to set DEBIAN_VERSION
  * debian/rules: Use dpkg-recommended build flags (Closes: #767531)
  * Remove command aliases without the 'sgt-' prefix (Closes: #684193)
  * towers.c: Fix FTBFS with gcc 6 (Closes: #811577)
  * debian/rules: Disable running tests; upstream tarballs don't include the
    necessary files
  * Add desktop files and Debian menu entries for the new puzzles
  * mkmanpages.pl: Fix regexp syntax warnings
  * mkmanpages.pl: Update for new version format
  * Change help browser search path to xdg-open:sensible-browser, since yelp
    and khelpcenter no longer work with arbitrary HTML files
  * Use msgmerge --previous option when updating .po files, thanks to
    Helge Kreutzmann
  * Update German translation, thanks to Helge Kreutzmann

 -- Ben Hutchings <ben@decadent.org.uk>  Wed, 20 Jul 2016 01:34:44 +0100

sgt-puzzles (20140928.r10274-1) unstable; urgency=medium

  * New upstream version
    - Version scheme is now date-based
    - Fix a failure to warn about non-unique rows/columns in non-square Unruly
      grids (Closes: #718354)
  * Add Debian menu entries for Range and Signpost
  * debian/watch: Delete, as new versions are date-based and there is
    currently no obvious way to find the last date changed
  * Disable 304_combine-binaries.diff which no longer applies
  * Build using autotools
    - Drop patches 301_fix-install-dirs.diff, 305_no-werror.diff
  * Exclude puzzles.chm from orig tarball because it requires non-free tools
    to regenerate from source
  * Update policy version to 3.9.6; no changes required

 -- Ben Hutchings <ben@decadent.org.uk>  Wed, 01 Oct 2014 22:10:50 +0100

sgt-puzzles (9872-1) unstable; urgency=low

  * New upstream version
    - Add an explicit -lm to the link lines in Makefile.gtk (Closes: #713476)
    - Add Undead by Steffen Bauer, an implementation of 'Haunted Mirror Maze'
    - Add Unruly by Lennard Sprong, an implementation of a puzzle usually
      called 'Tohu wa Vohu'
  * Add DEP-3 headers to patches
  * pearl: Require width or height to be at least 6 for Tricky
    (Closes: #667963)
  * debian/watch: Update ViewVC URL regex
  * Add 'sgt-' prefix to all command names and remove 'game' suffix, but
    retain symlinks under the old names (see #684193)
  * Use upstream short descriptions in English manual pages and package
    description
  * Update German translation, thanks to Helge Kreutzmann

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 30 Jun 2013 03:20:16 +0100

sgt-puzzles (9411-1) unstable; urgency=low

  * New upstream version - closes: #666709
    - Adds Pearl puzzle
  * Update German translation, thanks to Helge Kreutzmann

 -- Ben Hutchings <ben@decadent.org.uk>  Sat, 07 Apr 2012 02:38:40 +0100

sgt-puzzles (9306-1) unstable; urgency=low

  * New upstream version
  * Update German translation, thanks to Helge Kreutzmann
  * Update policy version to 3.9.2; no changes required
  * Update description to include the puzzles added in 8853-1 and 9109-1

 -- Ben Hutchings <ben@decadent.org.uk>  Mon, 12 Dec 2011 02:40:18 +0000

sgt-puzzles (9179-1) unstable; urgency=low

  * New upstream version:
    - Remove unused-but-set variables - closes: #625425
    - Avoid infinite loop in Loopy at Easy level
    - Add Penrose tilings to Loopy
  * Update German translation, thanks to Helge Kreutzmann
  * Do not compile with -Werror

 -- Ben Hutchings <ben@decadent.org.uk>  Mon, 11 Jul 2011 03:56:55 +0100

sgt-puzzles (9109-1) unstable; urgency=low

  * New upstream version:
    - Add Range and Signpost puzzles
    - Use stock icons and conventional order for dialog buttons
    - Use Cairo for screen rendering
  * Update German translation, thanks to Helge Kreutzmann
  * Remove or update patches applied or partially applied upstream
  * Use Debian source format 3.0 (quilt)

 -- Ben Hutchings <ben@decadent.org.uk>  Tue, 01 Mar 2011 04:16:54 +0000

sgt-puzzles (8853-3) unstable; urgency=low

  * Update German translation, thanks to Helge Kreutzmann
  * Fix reference to puzzles.txt at the bottom of manual pages
  * Remove obsolete patch 102_fix-bridges-min-sensible-islands.diff
  * Flag 5 / 2 = 2 as an error in Keen - closes: #581445

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 08 Aug 2010 23:34:32 +0100

sgt-puzzles (8853-2) unstable; urgency=low

  * Correct minor documentation errors, thanks to Helge Kreutzmann
    - closes: #571975
  * Document Helge Kreutzmann's copyright - closes: #571976
  * Document my copyright
  * Update German translation, thanks to Helge Kreutzmann
  * Fix minor issues reported by lintian:
    - Add ${misc:Depends} to dependencies
    - Add debian/source/format file
    - Use debhelper 7
  * Update policy version to 3.8.4; no changes required

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 04 Apr 2010 16:15:16 +0100

sgt-puzzles (8853-1) unstable; urgency=low

  * New upstream version - closes: #569901
  * Update German translation, thanks to Helge Kreutzmann

 -- Ben Hutchings <ben@decadent.org.uk>  Sat, 20 Feb 2010 21:41:07 +0000

sgt-puzzles (8786-1) unstable; urgency=low

  * New upstream version
    - Correct minor documentation errors - closes: #522439, #548472
  * Update German translation, thanks to Helge Kreutzmann
  * Correct minor documentation errors, thanks to Helge Kreutzmann
    - closes: #554341

 -- Ben Hutchings <ben@decadent.org.uk>  Mon, 15 Feb 2010 14:33:45 +0000

sgt-puzzles (8692-1) unstable; urgency=low

  * New upstream version
  * Add German descriptions to desktop files, thanks to Helge Kreutzmann
  * Update debian/watch file

 -- Ben Hutchings <ben@decadent.org.uk>  Mon, 02 Nov 2009 01:34:59 +0000

sgt-puzzles (8605-2) unstable; urgency=low

  * Apply fixes from Ubuntu - closes: #543527
    - Fix typo in French description of netslide, thanks to Didier Roche
  * Shade filled squares in Slant - closes: #419836
  * Update German translation, thanks to Helge Kreutzmann

 -- Ben Hutchings <ben@decadent.org.uk>  Wed, 30 Sep 2009 22:54:12 +0100

sgt-puzzles (8605-1) unstable; urgency=low

  * New upstream release
  * Force use of bash in debian/rules - closes: #535418
  * Update German translation, thanks to Helge Kreutzmann
  * Fix generation of translated manual pages - closes: #528042 (again)
    - Do not require the associated locale to be installed at build time
    - Tell Halibut that the input and output files are UTF-8-encoded

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 16 Aug 2009 19:26:56 +0100

sgt-puzzles (8541-2) unstable; urgency=low

  * Update German translation, thanks to Helge Kreutzmann
  * Install translated online help and manual pages - closes: #528042
  * Use g_spawn_async() to run help browser
    - Avoids zombie processes - closes LP: #385149
    - Avoids showing an error box in the child process, which would
      cause the parent to abort

 -- Ben Hutchings <ben@decadent.org.uk>  Sat, 13 Jun 2009 04:37:18 +0100

sgt-puzzles (8541-1) unstable; urgency=low

  * New upstream release
  * Add partial German translation of documentation - closes: #522438
  * Fix command in netslide desktop file - closes LP: #272942
  * Update policy version to 3.8.1; no changes required

 -- Ben Hutchings <ben@decadent.org.uk>  Tue, 05 May 2009 00:26:46 +0100

sgt-puzzles (8446-1) unstable; urgency=low

  * New upstream release
    - Obsoletes patches 101, 103, 204
    - Adds test for fwrite() failure - closes: #505157
    - Fixes bug in the Light Up solver
    - Adds keyboard control to many puzzles - closes: #417547
  * Remove obsolete patch to Unequal hit detection - closes: #501197
  * Update policy version to 3.8.0:
    - Add README.source
  * Add Vcs-* fields for my public repository
  * Add support for translation of the documentation - closes: #483665
  * Fix gcc 4.4 warnings - closes: #505359
    - Add 106_fix-uninit-warning.diff
    - Other warning fixed upstream
  * Add .desktop files, thanks to Didier Roche - closes: #495561

 -- Ben Hutchings <ben@decadent.org.uk>  Mon, 16 Feb 2009 01:03:44 +0000

sgt-puzzles (7983-1) unstable; urgency=low

  * New upstream release
  * Moved common code into a private shared library, halving package
    size and installed size
  * Implemented highlighting of more kinds of error in Loopy
  * Changed encoding of copyright file to UTF-8
  * Updated standards-version to 3.7.3 (no other changes required)

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 13 Apr 2008 17:39:38 +0100

sgt-puzzles (7703-1) unstable; urgency=low

  * New upstream release
  * Moved changes into patches applied with quilt
  * Removed dependency of online help on GNOME - closes: #429864, #437962

 -- Ben Hutchings <ben@decadent.org.uk>  Sat,  6 Oct 2007 23:36:20 +0100

sgt-puzzles (7636-1) unstable; urgency=low

  * New upstream release

 -- Ben Hutchings <ben@decadent.org.uk>  Sun,  8 Jul 2007 01:36:53 +0100

sgt-puzzles (7446-1) unstable; urgency=low

  * New upstream release - closes: #417543
  * Corrected minimum number of islands for Bridges - closes: #417541

 -- Ben Hutchings <ben@decadent.org.uk>  Tue, 24 Apr 2007 21:31:11 +0100

sgt-puzzles (7407-1) unstable; urgency=low

  * New upstream release adds Filling, Galaxies and Unequal puzzles
    - closes: #416009
  * Reverted to default version of gcc since bug #380541 has been fixed

 -- Ben Hutchings <ben@decadent.org.uk>  Tue, 27 Mar 2007 01:30:20 +0100

sgt-puzzles (6879-1) unstable; urgency=low

  * New upstream release
  * Changed window sizing to remember tile size when the puzzle type
    is changed - closes: #379452
  * Documented configuration variables - closes: #375055
  * Changed compiler to gcc-3.3 - closes: #391273

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 29 Oct 2006 02:38:36 +0000

sgt-puzzles (6844-2) unstable; urgency=low

  * Added compiler version dependency since gcc-4.0 is no longer in
    build-essential and previous version FTBFS in many places

 -- Ben Hutchings <ben@decadent.org.uk>  Wed,  6 Sep 2006 14:42:53 +0100

sgt-puzzles (6844-1) unstable; urgency=low

  * New upstream release
  * Added recommendation of yelp
  * Changed to build with gcc-4.0 - closes: #380455
  * Added missing .R files from upstream svn

 -- Ben Hutchings <ben@decadent.org.uk>  Tue,  5 Sep 2006 23:44:13 +0100

sgt-puzzles (6739-1) unstable; urgency=low

  * New upstream release
  * Moved help files under /usr/share/sgt-puzzles - closes: #379876
  * Changed help invocation and added sym-link to work around help search
    path changes in libgnome - closes: #379527
  * Added error checking and reporting to help invocation
  * Updated maintainer address

 -- Ben Hutchings <ben@decadent.org.uk>  Sat, 24 Jun 2006 01:21:49 +0100

sgt-puzzles (6616-1) unstable; urgency=low

  * New upstream version

 -- Ben Hutchings <ben@decadentplace.org.uk>  Wed, 22 Mar 2006 23:53:09 +0000

sgt-puzzles (6526-1) unstable; urgency=low

  * New upstream version
  * Fixed clearing of window area - closes: #345024

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sun, 22 Jan 2006 12:30:00 +0000

sgt-puzzles (6452-5) unstable; urgency=low

  * Yet another attempt to fix intermittent manual page build failures

 -- Ben Hutchings <ben@decadentplace.org.uk>  Wed, 07 Dec 2005 02:07:34 +0000

sgt-puzzles (6452-4) unstable; urgency=low

  * Another attempt to fix intermittent manual page build failures

 -- Ben Hutchings <ben@decadentplace.org.uk>  Fri, 02 Dec 2005 00:54:52 +0000

sgt-puzzles (6452-3) unstable; urgency=low

  * Attempt to fix intermittent manual page build failures (bug #339407)

 -- Ben Hutchings <ben@decadentplace.org.uk>  Wed, 23 Nov 2005 02:31:26 +0000

sgt-puzzles (6452-2) unstable; urgency=low

  * Minor changes to package building

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sun, 13 Nov 2005 16:23:36 +0000

sgt-puzzles (6452-1) unstable; urgency=low

  * New upstream version fixes bug in Bridges puzzle
  * Enabled window resizing, closes #337802

 -- Ben Hutchings <ben@decadentplace.org.uk>  Fri, 11 Nov 2005 23:21:03 +0000

sgt-puzzles (6444-1) unstable; urgency=low

  * New upstream version adds Bridges and Tents puzzles, closes #335627
  * Updated command-line synopses in manual pages
  * Reorganised list of puzzles in package description

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sun, 06 Nov 2005 01:33:31 +0000

sgt-puzzles (6378-1) unstable; urgency=low

  * New upstream version
  * Include loopy puzzle

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sun, 09 Oct 2005 15:37:26 +0100

sgt-puzzles (6229-2) unstable; urgency=low

  * Add online help to the Help menu
  * Change Copy command to use the clipboard not the primary selection
  * Make menu accelerators visible

 -- Ben Hutchings <ben@decadentplace.org.uk>  Tue, 13 Sep 2005 09:01:28 +0100

sgt-puzzles (6229-1) unstable; urgency=low

  * New upstream version, closes #323683

 -- Ben Hutchings <ben@decadentplace.org.uk>  Mon, 29 Aug 2005 00:45:24 +0100

sgt-puzzles (6169-1) unstable; urgency=low

  * New upstream version
  * Improve generation of cross-references in manual pages

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sat, 06 Aug 2005 23:49:58 +0100

sgt-puzzles (6119-1) unstable; urgency=low

  * New upstream version
  * Change manual page generation to use upstream documentation as source

 -- Ben Hutchings <ben@decadentplace.org.uk>  Wed, 20 Jul 2005 09:09:10 +0100

sgt-puzzles (6085-2) unstable; urgency=low

  * Fix warning from gcc 4.0 treated as error

 -- Ben Hutchings <ben@decadentplace.org.uk>  Mon, 11 Jul 2005 22:30:52 +0300

sgt-puzzles (6085-1) unstable; urgency=low

  * New upstream version

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sun, 10 Jul 2005 17:07:15 +0300

sgt-puzzles (6039-1) unstable; urgency=low

  * New upstream version
  * Removed mistaken dependency on menu
  * Appended 'game' to names of 'flip' and 'net' to avoid confusion with programs
    of the same name in /usr/bin

 -- Ben Hutchings <ben@decadentplace.org.uk>  Fri, 01 Jul 2005 00:56:01 +0100

sgt-puzzles (6009-1) unstable; urgency=low

  * New upstream version
  * Added dependency on menu
  * Fixed potential for breakage in build rules
  * Minor style fixes

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sat, 25 Jun 2005 01:20:47 +0100

sgt-puzzles (5982-2) unstable; urgency=low

  * Add menu entries

 -- Ben Hutchings <ben@decadentplace.org.uk>  Fri, 24 Jun 2005 23:57:54 +0100

sgt-puzzles (5982-1) unstable; urgency=low

  * Initial upload, closes: #309175

 -- Ben Hutchings <ben@decadentplace.org.uk>  Fri, 24 Jun 2005 03:53:02 +0100
