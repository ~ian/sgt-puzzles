/*
 * Puzzles version numbering.
 */

#include "version.h"

char ver[] = VER
#if defined DEBIAN_VERSION
    " (Debian package " DEBIAN_VERSION ")"
#endif
    ;
